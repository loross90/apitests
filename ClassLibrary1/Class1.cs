﻿using System;
using NUnit.Framework;
using RestSharp;
using System.Text.RegularExpressions;

namespace ClassLibrary1
{
    [TestFixture]
    public class Class1
    {
        [Test]
        public void Test1()
        {
            RestClient client = new RestClient("https://reqres.in/");
            RestRequest request = new RestRequest("api/users?page=1", Method.GET);
            request.ReadWriteTimeout = 2000;
            IRestResponse response = client.Execute(request);
            Assert.That(new Regex("\"id\"").Matches(response.Content).Count,Is.EqualTo(6));
        }
    }
}
