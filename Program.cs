﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Deserializers;
using System.Text.RegularExpressions;
using NUnit.Framework;
using System.Text.Json;
using System.IO;
using System.Text.Json.Serialization;


namespace APITest
{
    class Program
    {
        //В Main были небольшие тесты подключения и десериализации. В принципе все тесты ниже не зависят от Main 
        //и поэтому Main может быть полностью закомментирован
        private const string BASE_URL = "https://reqres.in/";
        public static void Main(string[] args)
        {
            RestClient client = new RestClient(BASE_URL);
            RestRequest request = new RestRequest("api/users/23", Method.GET);
            request.ReadWriteTimeout = 2000;
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.StatusCode);
            Console.WriteLine(new Regex("\"id\"").Matches(response.Content).Count);
            User Person = new User() {data=new Data() {id = 2, avatar = "aa",
                email = "as", first_name = "asd", last_name = "awe" }, 
                ad = new Ad() {url = "aas", company = "asd", description = "ssd" }, 
                support = new Support() {text = "asd", url = "asd" } };
            Users Person1 = (JsonSerializer.Deserialize<Users>(response.Content));
            Console.WriteLine(Person1.data[1].id);
        }
    }

    [TestFixture]
    public class Class1
    {
        public RestClient client = new RestClient("https://reqres.in/");

        // Тест на то что при запросе страницы n будет возвращена сущность с номером страницы n
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(10)]
        public void Test1(int pageNumber)
        {
            RestRequest request = new RestRequest(String.Format("api/users?page={0}",pageNumber), Method.GET);
            request.ReadWriteTimeout = 2000;
            IRestResponse response = client.Execute(request);
            Assert.That(JsonSerializer.Deserialize<Users>(response.Content).page, Is.EqualTo(pageNumber));
        }

        //Тест на то, что при выполнении недопустимого запроса, не будет статуса OK в результате выполнения при получении страницы с пользователями
        [TestCase("-1")]
        [TestCase("string")]
        public void Test2(string str)
        {
            RestRequest request = new RestRequest(String.Format("api/users?page={0}", str), Method.GET);
            request.ReadWriteTimeout = 2000;
            IRestResponse response = client.Execute(request);
            Assert.That(response.StatusCode.ToString, Is.Not.EqualTo("OK"));
        }
        
        //Тест на то, что при выполнении недопустимого запроса, не будет статуса OK в результате выполнения при получении информации о конкретном пользователе
        [TestCase("-1")]
        [TestCase("string")]
        public void Test7(string str)
        {
            RestRequest request = new RestRequest(String.Format("api/users/{0}", str), Method.GET);
            request.ReadWriteTimeout = 2000;
            IRestResponse response = client.Execute(request);
            Assert.That(response.StatusCode.ToString, Is.Not.EqualTo("OK"));
        }
        // Тест на то, что при запросе возвращается сущность с тем id, который запрашивался
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(10)]
        public void Test3(int userId)
        {
            RestRequest request = new RestRequest(String.Format("api/users/{0}", userId), Method.GET);
            request.ReadWriteTimeout = 2000;
            IRestResponse response = client.Execute(request);
            Assert.That(JsonSerializer.Deserialize<User>(response.Content).data.id, Is.EqualTo(userId));
        }


        //Тест на проверку того, что реальное количество пользователей на странице соответствует заявленному
        [Test]
        public void Test6()
        {
            RestRequest request = new RestRequest("api/users?page=1", Method.GET);
            request.ReadWriteTimeout = 2000;
            IRestResponse response = client.Execute(request);
            int totalPages = JsonSerializer.Deserialize<Users>(response.Content).total_pages;
            for (int i = 1; i <= totalPages; i++)
            {
                request = new RestRequest(String.Format("api/users?page={0}", i), Method.GET);
                response = client.Execute(request);
                Assert.That(JsonSerializer.Deserialize<Users>(response.Content).per_page,
                Is.EqualTo(JsonSerializer.Deserialize<Users>(response.Content).data.Length));
            }
        }


        // Тест в большей степени на Single User Not Found, но в целом он относится
        //к Single User поскольку ожидаемое поведение (NotFound) от метода получения Single User должно совпадать с реальным
        [TestCase(21)]
        [TestCase(23)]
        [TestCase(100)]
        public void Test4(int userId)
        {
            RestRequest request = new RestRequest(String.Format("api/users/{0}", userId), Method.GET);
            request.ReadWriteTimeout = 2000;
            IRestResponse response = client.Execute(request);
            Assert.That(response.StatusCode.ToString, Is.EqualTo("NotFound"));
        }

        //Тест на то, что у всего заявленного количества пользователей заполнены имя и фамилия и email
        [Test]
        public void Test5()
        {
            RestRequest request = new RestRequest("api/users?page=1", Method.GET);
            request.ReadWriteTimeout = 2000;
            IRestResponse response = client.Execute(request);
            int totalUsers = JsonSerializer.Deserialize<Users>(response.Content).total;
            for (int i = 1; i <= totalUsers; i++)
            {
                request = new RestRequest(String.Format("api/users/{0}", i), Method.GET);
                request.ReadWriteTimeout = 2000;
                response = client.Execute(request);
                Assert.That(!String.IsNullOrEmpty(JsonSerializer.Deserialize<User>(response.Content).data.first_name.ToString()));
                Assert.That(!String.IsNullOrEmpty(JsonSerializer.Deserialize<User>(response.Content).data.last_name.ToString()));
                Assert.That(!String.IsNullOrEmpty(JsonSerializer.Deserialize<User>(response.Content).data.email.ToString()));
            }
        }
    }
}
