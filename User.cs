﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APITest
{
    class User
    {
        public Data data { get; set; }
        public Support support { get; set; }
        public Ad ad { get; set; }
    }

    class Users
    {
        public int page { get; set; }
        public int per_page { get; set; }
        public int total { get; set; }
        public int total_pages { get; set; }

        public Data[] data { get; set; }
        public Support support { get; set; }
        public Ad ad { get; set; }
    }
    class Data
    {
        public int id { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string avatar { get; set; }
    }

    class Support
    {
        public string url { get; set; }
        public string text { get; set; }
    }

    class Ad
    {
        public string company { get; set; }
        public string description { get; set; }
        public string url { get; set; }
    }
}
